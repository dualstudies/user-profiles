import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.scss']
})
export class SignUpComponent implements OnInit {
  registrationForm: FormGroup;
  isSubmitted: boolean;
  constructor(public fb: FormBuilder) {
  }

  ngOnInit() {
    this.isSubmitted = false;
    this.registrationForm = this.fb.group({
      fname: [null, Validators.required],
      lname: [null, Validators.required],
      address: [null, Validators.required],
      phone: [null, Validators.required],
      email: [null, Validators.required],
      pass: [null, Validators.required],

    });

  }

  submitForm() {
    this.isSubmitted = true;
    if (!this.registrationForm.invalid) {
      console.log(this.registrationForm.value);
    }
  }
}

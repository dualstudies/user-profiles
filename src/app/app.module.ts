import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { AppComponent } from './app.component';
import { CardComponent } from './card/card.component';
import { DatePipe } from '@angular/common';
import { UserService } from './services/userServices/user.service';
import {InfiniteScrollModule} from 'ngx-infinite-scroll';
import { SignUpComponent } from './sign-up/sign-up.component';
import { LogInComponent } from './log-in/log-in.component';
import { AppRoutingModule } from './app-routing.module';
import { FormsModule } from '@angular/forms';
import { HeaderComponent } from './header/header.component';
import { ProfileComponent } from './profile/profile.component';
import { AgmCoreModule } from '@agm/core';
import { ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    AppComponent,
    CardComponent,
    SignUpComponent,
    LogInComponent,
    HeaderComponent,
    ProfileComponent,
  ],
  entryComponents: [
    CardComponent
  ],
  imports: [
    ReactiveFormsModule,
    HttpClientModule,
    BrowserModule,
    InfiniteScrollModule,
    AppRoutingModule,
    FormsModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyBUkR0Q6HprCdus0WgnmiCVt2A0LuBdCk4',
      libraries: ['places']

    })
  ] ,
  providers: [
    HttpClientModule,
    HttpClient,
    DatePipe,
    UserService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}

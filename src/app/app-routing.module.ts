import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SignUpComponent } from './sign-up/sign-up.component';
import { LogInComponent } from './log-in/log-in.component';
import { CardComponent } from './card/card.component';
import { ProfileComponent } from './profile/profile.component';
import { RouterModule, Routes } from '@angular/router';



const routes: Routes = [
  { path: '', redirectTo: '/users', pathMatch: 'full', },
  { path: 'login', component: LogInComponent },
  { path: 'users', component: CardComponent },
  { path: 'profile', component: ProfileComponent },
  { path: 'signUp', component: SignUpComponent }];

@NgModule({
  declarations: [],
  imports: [RouterModule.forRoot(routes),
    CommonModule],
  exports: [ RouterModule ]
})
export class AppRoutingModule { }

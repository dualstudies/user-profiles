import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {Observable} from 'rxjs';

const API_URL = 'https://randomuser.me/api/';
// const USERS = [];

@Injectable({
  providedIn: 'root'
})
export class UserService {
  constructor(public http: HttpClient) {
  }

  getUsers(page, results): Observable<any> {
    return this.http.get(API_URL + '?page=' + page + '&results=' + results);
  }

  // getUser(uuid): any {
  //   return USERS.map((user) => {
  //     return user.login.uuid === uuid;
  //   })[0];
  // }
}

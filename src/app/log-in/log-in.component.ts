import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-log-in',
  templateUrl: './log-in.component.html',
  styleUrls: ['./log-in.component.scss']
})
export class LogInComponent implements OnInit {
  registrationForm: FormGroup;
  isSubmitted: boolean;

  constructor(public fb: FormBuilder) {
  }


  ngOnInit() {
    this.isSubmitted = false;
    this.registrationForm = this.fb.group({
      username: [null, Validators.required],
      password: [null, Validators.required],


    });

  }
  submitForm() {
    this.isSubmitted = true;
    if (!this.registrationForm.invalid) {
      console.log(this.registrationForm.value);
    }
  }

}

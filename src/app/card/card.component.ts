import { Component, OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { UserService } from '../services/userServices/user.service';
import {Router} from '@angular/router';


@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit {
  page: number;
  results: number;
  users: any[];
  isLoading: boolean;

  constructor(public datepipe: DatePipe,
              public userService: UserService,
              public router: Router) {
    this.page = 1;
    this.results = 9;
    this.users = [];
    this.isLoading = false;
  }

  ngOnInit() {
     this.loadUsers();

  }

  setInfo(user, type) {
    const TITLES = {
      name: 'My name is',
      dob: 'My birthday',
      email: 'My e-mail is',
      address: 'I\'m from',
      gender: 'My gender is',
      phone: 'My number',
    };
    const field = TITLES[type];
    user.currentTitle = TITLES[type];
    user.currentData = user[type];

    if (type === 'name') {
      user.currentData = user.name.title + ': ' + user.name.first + ' ' + user.name.last;
    }

    if (type === 'dob') {
      user.currentData = this.datepipe.transform(user.dob.date, 'mm/dd/yyyy');
    }
    if (type === 'address') {
      user.currentData = user.location.city;

    }
  }

  onScroll() {
    this.loadUsers();
  }

  loadUsers() {
    this.isLoading = true;
    this.userService.getUsers(this.page, this.results)
      .subscribe(data => {
        if (this.page === 1) {
          this.users = data.results;
        } else {
          this.users = this.users.concat(data.results);
        }
        this.isLoading = false;
        this.page++;
      });
  }

  navigateToProfile(data) {
    this.router.navigateByUrl('/profile', { state: { user: data } });
  }

}


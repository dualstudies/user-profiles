import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {map} from 'rxjs/operators';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  user: any;
  lat: number;
  long: number;
  zoom: number;

  constructor(private route: ActivatedRoute,
              private router: Router
  ) {
  }

  ngOnInit() {
    this.zoom = 3;
    this.route.paramMap
      .pipe(map(() => window.history.state)).subscribe(data => {
      if (!data.user) {
        this.router.navigateByUrl('');
      }
      this.user = data.user;
      this.lat = parseFloat(this.user.location.coordinates.latitude);
      this.long = parseFloat(this.user.location.coordinates.longitude);
    });
  }

}
